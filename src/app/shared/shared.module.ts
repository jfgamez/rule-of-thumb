import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { NavBarComponent } from "./nav-bar/nav-bar.component";
import { BannerComponent } from "./banner/banner.component";
import { CardsComponent } from "./cards/cards.component";
import { FooterComponent } from './footer/footer.component';
import { AlertComponent } from './alert/alert.component';
import { AskBannerComponent } from './ask-banner/ask-banner.component';

@NgModule({
  declarations: [
    NavBarComponent,
    BannerComponent,
    CardsComponent,
    FooterComponent,
    AlertComponent,
    AskBannerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NavBarComponent,
    BannerComponent,
    CardsComponent,
    FooterComponent,
    AlertComponent,
    AskBannerComponent
  ]
})
export class SharedModule {}

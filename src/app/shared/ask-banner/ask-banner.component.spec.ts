import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AskBannerComponent } from './ask-banner.component';

describe('AskBannerComponent', () => {
  let component: AskBannerComponent;
  let fixture: ComponentFixture<AskBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AskBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input } from "@angular/core";

@Component({
  selector: "app-cards",
  templateUrl: "./cards.component.html",
  styleUrls: ["./cards.component.scss"],
})
export class CardsComponent {
  // data input from votes
  @Input() info: any;

  // flag no know action
  isLikeFg:boolean = false;
  isUnLikeFg:boolean = false;

  // Calc total
  totalVotes: number = 0;
  likePorcent: String = "0";
  unLikePorcent: String = "0";
  
  /**
   * Implement OnInit in the component
   */
  ngOnInit() {
    this.calcValuesVotes();
  }

  /**
   * To calcule votes from input
   * @returns void
   */
  calcValuesVotes() :void {
    this.totalVotes =  this.info.upVotes  +  this.info.downVotes;
    this.likePorcent = ((this.info.upVotes / this.totalVotes)*100).toFixed(0);
    this.unLikePorcent =  ((this.info.downVotes / this.totalVotes)*100).toFixed(0);
  }
  /**
   * To handle flags when is like
   * @returns void
   */
  onLike(): void {
    this.isUnLikeFg = false;
    this.isLikeFg = true;
  }
  /**
   * To handle flags when is unlike
   * @returns void
   */
  onUnLike(): void {
    this.isLikeFg = false;
    this.isUnLikeFg = true;
  }

  /**
   * Allow adding votes
   * @param  {number} id
   * @returns void
   */
  OnVote(id: number): void {
    let rankingTmp:any[] = JSON.parse(localStorage.getItem("ranking"));

    for (let i = 0; i < rankingTmp.length; i++){
      if (rankingTmp[i].id == id){

        if (this.isLikeFg) {
          rankingTmp[i].upVotes = rankingTmp[i].upVotes + 1;
          this.info.upVotes = rankingTmp[i].upVotes;
        } else {
          rankingTmp[i].downVotes = rankingTmp[i].downVotes + 1;
          this.info.downVotes = rankingTmp[i].downVotes;
        }
        localStorage.setItem("ranking", JSON.stringify(rankingTmp));
        this.calcValuesVotes();
        break;
      }
    }
  }
}

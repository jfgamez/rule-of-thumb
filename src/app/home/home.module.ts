//Modules from core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Modules
import { SharedModule } from "../shared/shared.module";

//Components
import { HomeComponent } from "./components/home.component";
import { VotesComponent } from './components/votes/votes.component';

@NgModule({
  declarations: [
    HomeComponent,
    VotesComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }

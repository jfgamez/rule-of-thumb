import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  /**
   * To Inject modules in service 
   * @param  {HttpClient} privatehttp
   */
  constructor(private http: HttpClient) {}

  /**
   * To get data from public figure json for votes 
   * @returns Observable
   */
  public getPublicFigureInfo(): Observable<any> {
    return this.http.get("../../../assets/data/publicFigureInfo.json");
  }

}

// Core module 
import { Component, OnInit } from '@angular/core';

//thirty party modules
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from "util";

//service
import { HomeService } from "../../service/home.service";

@Component({
  selector: 'app-votes',
  templateUrl: './votes.component.html',
  styleUrls: ['./votes.component.scss']
})
export class VotesComponent implements OnInit {
  // data info to cards
  private publicFigureInfo: any;

  // subscription to service
  private subscription: Subscription 

  /**
   * To inject home service in component
   * @param  {HomeService} privatehomeService
   */
  constructor(private homeService: HomeService) {}

  /**
   * To implement OnInit method in component
   */
  ngOnInit() {
    this.subscription = this.homeService.getPublicFigureInfo().subscribe(data => {
      this.LocalRanking(data);
    });
  }

  /**
   * Implements onDestroy to unsubscribe observables in component
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /**
   * To init the localstore ranking 
   * @param  {any} data
   * @returns any
   */
  LocalRanking(data: any): any {
    const ranking = localStorage.getItem("ranking");
    if (isNullOrUndefined(ranking)) {
      localStorage.setItem("ranking", JSON.stringify(data));
      this.publicFigureInfo = data;
    } else {
      this.publicFigureInfo = JSON.parse(ranking);
    }
  }

}

# Rule-of-thumb-jgamez-ui

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.

## Required

node.js installed, version >= 12 and < 14

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## How could I handle the parts of the exercise that I can't deliver

I would like to improve the testing. Also, i would like to refactor the code to handle the states with NGRX, and finish the part that the user clicks on the "Vote now” button, a message is displayed saying “Thank you for voting!” I didn't finish it
